#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa Servidor UDP que gestiona los mensajes enviados por los clientes
"""

import json
import socketserver
import sys
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ Es el "manejador" de los mensajes que el servidor recibe y
    los almacena en una estructura de datos """
    register = {}

    def json2registered(self):
        """ Se encarga de restaurar la lista de clientes desde
        la ultima conexion (almacenados en un json)"""
        try:
            outfile = open("registered.json", 'r')
            self.register = json.load(outfile)
            outfile.close()

        except ValueError:
            self.register = {}

        except FileNotFoundError:
            outfile = open("registered.json", 'w')
            outfile.close()
            self.register = {}

    def register2json(self):
        """ Se encarga de restaurar la lista de
        clientes desde la ultima conexion"""
        with open("registered.json", 'w') as outfile:
            json.dump(self.register, outfile, indent=2)

    def handle(self):
        """ Esta funcion es la encargada de gestionar el envio
        y recepcion de los mensajes del cliente añadirlos
         a la estructura de datos,y gestionar cuando se borran"""
        print("IP Cliente: " + str(self.client_address[0]) +
              " Puerto Cliente: " + str(self.client_address[1]))
        message = ""
        for line in self.rfile:
            if line.decode('utf-8') != '\r\n':
                message += line.decode('utf-8')
        print("El cliente nos manda ", message)

        expire = int(message.split(" ")[-1])
        expireDate = time.time() + expire
        self.register[message.split(" ")[1][4:]] = \
            {"address": self.client_address[0],
             "expires": time.strftime('%Y-%m-%d %H:%M:%S',
                                      time.gmtime(expireDate))}

        for i in list(self.register):
            now = time.gmtime(time.time())
            now_sec = (
                    (now[0] - 2000) * 31536000 + now[1] * 2.628e+6
                    + now[2] * 86400 + 3600 * now[3] + 60 * now[4] + now[5])

            time_i = time.strptime(self.register[i]["expires"],
                                   '%Y-%m-%d %H:%M:%S')
            time_sec = ((time_i[0] - 2000) * 31536000 + time_i[1] * 2.628e+6
                        + time_i[2] * 86400 + 3600 * time_i[3]
                        + 60 * time_i[4] + time_i[5])

            if now_sec >= time_sec:
                del self.register[i]

        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        self.register2json()


if __name__ == "__main__":

    try:
        PORT = int(sys.argv[1])
        print("Lanzando servidor UDP de eco...")
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        SIPRegisterHandler.json2registered(SIPRegisterHandler)
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
    except IndexError:
        print("Introduce el puerto del cliente")
    except ValueError:
        print("Introduce un numero de puerto valido")
