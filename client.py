#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

try:
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    if sys.argv[3] == "register":
        LINE = "REGISTER " + 'sip:' + (sys.argv[4]) + " SIP/2.0\r\n" + \
               "Expires: " + sys.argv[5]
    else:
        raise Exception

except (IndexError, ValueError, Exception):
    sys.exit("Usage: client.py ip puerto register sip_address expires_value")

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
